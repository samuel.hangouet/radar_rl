import gym
import numpy as np
from gin import configurable
from optuna import Trial

from agent import AgentBase


@configurable
class RandomAgent(AgentBase):

    @staticmethod
    def hp_search(trial: Trial) -> dict:
        return {}

    def begin_episode(self, obs: np.ndarray, training_mode: bool):
        return self.action_space.sample()

    def step(self, reward: float, new_obs: np.ndarray):
        return self.action_space.sample()

    def add_training_experience(self, obs, action, reward, next_obs, done):
        pass

    def end_episode(self, reward: float, final_obs: np.ndarray):
        pass
