from collections import deque
from math import pi
from random import Random
from typing import List, Set, Tuple, Deque, Optional, Sequence

from gin import configurable
from pymunk import Vec2d


class Echo:
    def __init__(self, date: int, position: Vec2d, radial_velocity: Vec2d):
        self.date = date
        self.position = position
        self.radial_velocity = radial_velocity


@configurable
class Plane:
    def __init__(self,
                 position: Vec2d,
                 velocity: Vec2d,
                 rotation_per_sec=0.05,  # standard deviation of rotation angle per second, rad/s
                 straightness_factor=0.99,  # 0 = keep initial velocity, 1 = rotate at will
                 ):
        self.rotation_std = rotation_per_sec
        self.straightness = straightness_factor
        self.position = position
        self.last_position = position
        self.velocity = velocity
        self.last_rotation = 0
        self.last_seen_position: Optional[Vec2d] = None

    def move(self, re: Random, elapsed: float):
        # Random walk with integrated turning angle
        self.last_position = self.position
        self.last_rotation = re.gauss(self.last_rotation * self.straightness, self.rotation_std * elapsed)
        self.velocity = self.velocity.rotated(self.last_rotation)
        self.position += self.velocity * elapsed


@configurable
class Radar:

    def __init__(self,
                 position: Vec2d,
                 orientation: Vec2d,
                 max_range: float = 550,
                 min_range: float = 50,
                 max_angular_speed=2,  # rad.s-1
                 max_echos=100):
        self.position = position
        self.orientation = orientation
        self.last_orientation = orientation
        self.echos: Deque[Echo] = deque(maxlen=max_echos)
        self.max_angular_speed = max_angular_speed
        self.max_range = max_range
        self.min_range = min_range

    def rotate(self, direction: float, elapsed: float):
        """
        :param elapsed: step duration in seconds
        :param direction: [-1, 1] rotation order (1=full speed clockwise, 0=keep orientation)
        """
        self.last_orientation = self.orientation
        delta_angle = direction * self.max_angular_speed * elapsed
        self.orientation = self.orientation.rotated(delta_angle)

    def touched(self, plane: Plane) -> bool:
        v1 = plane.last_position - self.position
        v2 = plane.position - self.position

        # Check if the beam intersected the trajectory of the plane during last step.
        # To do that, we compute the angle between the first and last position of beam during the step
        # and the first and last position of the planes. If at least two of these four angles have an opposite sign,
        # then the beam has been touched.
        a1 = self.orientation.get_angle_between(v1)
        if abs(a1) > pi * 0.5:
            return False
        a2 = self.orientation.get_angle_between(v2)
        if a1 * a2 < 0:
            return True

        b1 = self.last_orientation.get_angle_between(v1)
        b2 = self.last_orientation.get_angle_between(v2)
        if b1 * b2 < 0:
            return True

        return b1 * a1 < 0

    def at_range(self, position: Vec2d):
        distance_squared = (position - self.position).get_length_sqrd()
        return self.min_range ** 2 < distance_squared < self.max_range ** 2

    def collect(self, plane: Plane, date: int):
        """Simulate echos for the planes seen by this radar. Waves propagation time is neglected.
        The given plane must be at range."""
        assert self.at_range(plane.position)

        if not self.touched(plane):
            return False

        # The received energy depends on the distance ** 4
        # We consider than the probability of detection is a linear function of this energy
        # distance = (plane.position - self.position).length
        # normalized_distance = (distance - self.min_range) / (self.max_range - self.min_range)
        # prob = (1 - normalized_distance) ** 4
        # if re.random_agent.py() > prob:
        #     return False

        radial_direction = (self.position - plane.position).normalized()
        radial_velocity = plane.velocity.dot(radial_direction) * radial_direction
        self.echos.append(Echo(date, plane.position, radial_velocity))
        return True


@configurable
class Scene:
    def __init__(self,
                 radar_placement_seed=0,
                 plane_spawn_seed=None,
                 num_radars=4,
                 fixed_positions=True,
                 size=2000,  # km
                 step_duration=0.1,  # s
                 total_duration=20,  # s
                 plane_min_speed=500,  # km.s-1
                 plane_max_speed=1000,
                 num_planes=20,
                 ):

        self.plane_max_speed = plane_max_speed

        self.num_steps = int(total_duration / step_duration)
        self.size = size
        self.step_duration = step_duration
        self.num_planes = num_planes

        self.planes: Set[Plane] = set()
        self.radars: List[Radar] = []

        self.re = Random(radar_placement_seed)
        if fixed_positions:
            self.place_4_radars_at_fixed_position(num_radars)
        else:
            self.place_radars_at_random(num_radars)

        self.spawn_schedule: List[Optional[Plane]] = [None] * self.num_steps
        self.re = Random(plane_spawn_seed)
        self.prepare_spawn(plane_min_speed, plane_max_speed)

        self.step = 0

        # Metrics
        self.detectable_sum = 0
        self.detected_sum = 0
        self.detectable_collab = 0
        self.detected_collab = 0

    @property
    def num_radars(self):
        return len(self.radars)

    def inside(self, pos: Vec2d):
        return (0 < pos.x < self.size) and (0 < pos.y < self.size)

    def prepare_spawn(self, plane_min_speed, plane_max_speed):
        num_planes = 0
        watchdog = 0
        while num_planes < self.num_planes:

            speed = plane_min_speed + self.re.random() * (plane_max_speed - plane_min_speed)
            steps_to_cross = round((self.size / speed) / self.step_duration)

            if steps_to_cross >= self.num_steps:
                raise ValueError("Not enough steps to cross the scene or planes too slow")

            step = self.re.randint(0, self.num_steps - steps_to_cross)
            if self.spawn_schedule[step] is not None:
                watchdog += 1
                if watchdog == 10000:
                    raise ValueError("Too much planes for episode length")
                continue
            watchdog = 0
            num_planes += 1
            p = self.re.random() * self.size
            position = self.re.choice([Vec2d(p, 0), Vec2d(p, self.size), Vec2d(0, p), Vec2d(self.size, p)])
            direction = Vec2d(0.5, 0.5) * self.size - position

            velocity = speed * direction / direction.length
            self.spawn_schedule[step] = Plane(position, velocity)

    def update(self, radar_orders=Sequence[float]):

        assert self.step < self.num_steps

        # Spawn planes on the borders of the scene
        new_plane = self.spawn_schedule[self.step]
        if new_plane is not None:
            self.planes.add(new_plane)

        # Rotate radars
        for radar, order in zip(self.radars, radar_orders):
            radar.rotate(order, self.step_duration)

        for plane in tuple(self.planes):

            # Move planes
            plane.move(self.re, self.step_duration)

            # Remove planes if out of the scene
            if not self.inside(plane.position):
                self.planes.remove(plane)
                continue

            # Collect echos
            detected = detectable = 0
            for radar in self.radars:
                if not radar.at_range(plane.position):
                    continue
                detectable += 1
                if radar.collect(plane, self.step):
                    plane.last_seen_position = plane.position
                    detected += 1

            self.detected_sum += detected
            self.detectable_sum += detectable
            self.detected_collab += (detected > 0)
            self.detectable_collab += (detectable > 0)

        self.step += 1
        return self.step == self.num_steps

    @property
    def metrics(self):
        return dict(detected_sum=self.detected_sum,
                    detectable_sum=self.detectable_sum,
                    detected_collab=self.detected_collab,
                    detectable_collab=self.detectable_collab)

    def place_4_radars_at_fixed_position(self, num_radars):

        assert num_radars <= 5, "fixed_positions only allowed up to 5 radars"
        self.radars = []
        r = Radar(Vec2d(0, 0), Vec2d(0, 0)).max_range
        positions = [Vec2d(r, r),
                     Vec2d(self.size - r, self.size - r),
                     Vec2d(self.size / 2, self.size / 2),
                     Vec2d(self.size - r, r),
                     Vec2d(r, self.size - r)]

        if num_radars == 1:
            positions = positions[2]
        elif num_radars == 2:
            positions = positions[:2]
        elif num_radars == 3:
            positions = positions[:3]
        elif num_radars == 4:
            positions = positions[:2] + positions[3:]

        for position in positions:
            orientation = Vec2d(1, 0).rotated(self.re.random() * 2 * pi)
            self.radars.append(Radar(position, orientation))

    def place_radars_at_random(self, num_radars):
        self.radars = []
        max_range = Radar(Vec2d(0, 0), Vec2d(0, 0)).max_range

        # Place radars with minimum separation and preventing their range from crossing the scene edges
        for _ in range(10):
            watch_dog = 0
            while len(self.radars) < num_radars:
                x = max_range + self.re.random() * (self.size - 2 * max_range)
                y = max_range + self.re.random() * (self.size - 2 * max_range)
                position = Vec2d(x, y)
                too_close = False
                for radar in self.radars:
                    distance_squared = radar.position.get_dist_sqrd(position)
                    if distance_squared < max_range ** 2:
                        too_close = True
                        break
                if too_close:
                    watch_dog += 1
                    if watch_dog == 1000:
                        break
                    continue
                watch_dog = 0
                orientation = Vec2d(1, 0).rotated(self.re.random() * 2 * pi)
                self.radars.append(Radar(position, orientation))

            if len(self.radars) == num_radars:
                return
            self.radars.clear()
        raise ValueError("Failed to place radars with given parameters")
