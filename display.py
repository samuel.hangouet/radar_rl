import sys
from random import random, choice

import pygame
from gin import configurable
from pymunk import Vec2d

from simulator import Scene


class View:
    """Converts env coordinates to window, keeping a margin."""

    def __init__(self, world_size):
        self.world_size = world_size
        self.window_width = self.window_height = 0
        self.x_margin = self.y_margin = 20
        self.zoom_factor = 1

    def adapt_to_window(self):
        self.x_margin = self.y_margin = 20
        self.window_width, self.window_height = pygame.display.get_surface().get_size()
        if self.window_width > self.window_height:
            self.zoom_factor = (self.window_height - 2 * self.y_margin) / self.world_size
            self.x_margin += (self.window_width - self.window_height) // 2
        else:
            self.zoom_factor = (self.window_width - 2 * self.y_margin) / self.world_size
            self.y_margin += (self.window_width - self.window_height) // 2

    def point2window(self, p: Vec2d):
        return p.x * self.zoom_factor + self.x_margin, p.y * self.zoom_factor + self.y_margin

    def vector2window(self, v: Vec2d):
        return v.x * self.zoom_factor, v.y * self.zoom_factor

    def size2window(self, s: float):
        return s * self.zoom_factor


@configurable
class Display:
    ECHOS = pygame.Color(44, 44, 44)
    BEAMS = pygame.Color(22, 200, 22)
    FILL_RANGE = pygame.Color(0, 0, 0)
    PLANES = pygame.Color(200, 0, 0)

    def __init__(self,
                 scene: Scene,
                 window_width=800,
                 window_height=800,
                 realtime=True,
                 title="Multi Radar Cooperation"):
        self._stepping = False
        self._pause = False
        self.scene = scene
        self._realtime = realtime
        self.view = View(scene.size)
        pygame.init()
        pygame.display.set_caption(title)
        pygame.key.set_repeat(500, 50)
        self.screen = pygame.display.set_mode((window_width, window_height), flags=pygame.RESIZABLE)
        self._clock = pygame.time.Clock()
        pygame.font.init()
        self._font = pygame.font.SysFont('Arial', 18)

    @property
    def paused(self):
        return self._pause

    def render_text(self, text, x, y, color):
        self.screen.blit(self._font.render(text, True, color), (x, y))

    def render_begin(self):
        self.view.adapt_to_window()
        self.screen.fill(pygame.Color(0,0,0,0))

    def render_end(self):
        pygame.display.flip()
        if self._realtime:
            self._clock.tick(1 / self.scene.step_duration)

    def step(self):
        self.render_begin()
        self.render_frame()
        self.render_end()
        self.handle_events()

    def render_frame(self):

        # Draw scene space
        top_left = self.view.point2window(Vec2d(0, 0))
        size = self.view.vector2window(Vec2d(self.scene.size, self.scene.size))
        pygame.draw.rect(self.screen, self.ECHOS, pygame.Rect(top_left, size))

        # Draw radar ranges
        for radar in self.scene.radars:
            pygame.draw.circle(self.screen,
                               self.FILL_RANGE,
                               self.view.point2window(radar.position),
                               self.view.size2window(radar.max_range))

        # Draw radar blind areas
        for radar in self.scene.radars:
            pygame.draw.circle(self.screen,
                               self.ECHOS,
                               self.view.point2window(radar.position),
                               self.view.size2window(radar.min_range))

        # Draw radar beams
        for radar in self.scene.radars:
            p1 = self.view.point2window(radar.position + radar.orientation * radar.max_range)
            p2 = self.view.point2window(radar.position + radar.last_orientation * radar.max_range)
            pygame.draw.polygon(self.screen, self.BEAMS, (self.view.point2window(radar.position), p1, p2))

        # Draw radar echos
        for radar in self.scene.radars:
            for echo in radar.echos:
                age = int(255 * (1 - (self.scene.step - echo.date) / self.scene.num_steps))
                color = pygame.Color(age, age, age)

                position = self.view.point2window(echo.position)
                arrow_end = self.view.point2window(echo.position + echo.radial_velocity * self.scene.step_duration)

                pygame.draw.line(self.screen, color, position, arrow_end)
                pygame.draw.circle(self.screen, color, position, 2)

        # Draw planes
        for plane in self.scene.planes:
            pygame.draw.circle(self.screen, self.PLANES, self.view.point2window(plane.position), 3)

        # Draw step number
        self.render_text(f'Step {self.scene.step}/{self.scene.num_steps}', 10, 10, pygame.Color(255, 0, 0))

    def handle_events(self):
        """Handles keyboard for pause, step and quit"""

        if self._stepping is True:
            self._pause = True
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                sys.exit(1)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    self._pause = not self._pause
                    self._stepping = self._pause
                if event.key == pygame.K_RIGHT:
                    self._pause = False


if __name__ == "__main__":
    def main():
        scene = Scene()
        display = Display(scene)

        orders = []
        for _ in scene.radars:
            orders.append(choice([-1, 0, 1]))

        finished = False
        while not finished:
            if not display.paused:
                finished = scene.update(orders)
            display.step()


    main()
