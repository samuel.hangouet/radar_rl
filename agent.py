import gym
import numpy as np
from optuna import Trial
from torch.utils.tensorboard import SummaryWriter


class AgentBase:
    """Base class for a RL agent."""

    def __init__(self, observation_space: gym.Space, action_space: gym.Space):
        self.observation_space = observation_space
        self.action_space = action_space
        self.writer = None

    @staticmethod
    def hp_search(trial: Trial) -> dict:
        raise NotImplementedError

    def set_writer(self, writer: SummaryWriter):
        self.writer = writer

    def begin_episode(self, obs: np.ndarray, training_mode: bool):
        """Returns action"""
        raise NotImplementedError

    def step(self, reward: float, new_obs: np.ndarray):
        """Returns action"""
        raise NotImplementedError

    def add_training_experience(self, obs, action, reward, next_obs, done):
        """Lets the agent fill an eventual replay buffer.
        Always called between begin_episode(training_mode=True) and end_episode.
        Observations arrays are copied before call, but obs can be the same instance of next_obs of previous call."""
        raise NotImplementedError

    def end_episode(self, reward: float, final_obs: np.ndarray):
        raise NotImplementedError

    def save(self, path: str):
        pass

    def load(self, path: str):
        pass
