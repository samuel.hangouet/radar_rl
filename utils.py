import heapq
import math
from glob import glob

import numpy as np
import torch


def sign(a):
    return (a > 0) - (a < 0)


def lerp(a, b, factor):
    """Linear interpolation"""
    return a * (1 - factor) + b * factor


def log_probs(probs):
    return torch.log(probs + 1e-9 * (probs == 0.0))


def multinomial(probs):
    # return np.argmax(np.random_agent.py.multinomial(1, probs))
    return torch.multinomial(torch.from_numpy(probs), 1).item()


def clamp(a, min_value, max_value):
    return max(min(a, max_value), min_value)


def mean_std(a):
    return np.mean(a), np.std(a)


def increment_path(path_prefix):
    n = 0
    for path in glob(f'{path_prefix}_*'):

        suffix = path[len(path_prefix) + 1:]
        try:
            n = max(n, int(suffix))
        except ValueError:
            pass
    n += 1
    return f'{path_prefix}_{n}'


class MeanStd:
    """On the flow computing of the variance and mean of a stream of values."""

    def __init__(self):
        self._k = 0
        self._n = 0
        self._ex = 0
        self._ex2 = 0

    def __len__(self):
        return self._n

    def mean(self):
        if self._n == 0:
            raise ValueError
        return self._k + self._ex / self._n

    def variance(self):
        if self._n <= 1:
            raise ValueError
        return (self._ex2 - self._ex * self._ex / self._n) / (self._n - 1)

    def std(self):
        return math.sqrt(1e-7 + self.variance())

    def push(self, x):
        if self._n == 0:
            self._k = x
        self._n += 1
        v = x - self._k
        self._ex += v
        self._ex2 += v * v

    def pop(self, x):
        if self._n == 0:
            raise ValueError
        self._n -= 1
        v = x - self._k
        self._ex -= v
        self._ex2 -= v * v


class MinHeap:
    def __init__(self):
        self.heap = []

    def clear(self):
        self.heap = []

    def push(self, item):
        """Adds an item into the heap"""
        heapq.heappush(self.heap, item)

    def pop(self):
        """Pops and returns the smallest item in the heap (use a tuple for multi-criterion sort)"""
        return heapq.heappop(self.heap)

    def peek(self):
        """Returns the smallest item but do not change the heap"""
        return self.heap[0]

    def __len__(self):
        return len(self.heap)
