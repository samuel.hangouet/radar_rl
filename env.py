from copy import copy
from itertools import chain
from typing import Tuple, Optional, Sequence

import numpy as np
import pygame
from gin import configurable
from pymunk import Vec2d
import gym

from display import Display
from simulator import Scene


@configurable
class ContinuousEnv(gym.Env):

    def __init__(self, grid_size=40):
        self._display: Optional[Display] = None
        self.scene = Scene()
        self._grid_size = grid_size
        self._observation = np.zeros(
            shape=(len(self.scene.radars), 3, grid_size, grid_size),
            dtype=np.float32)

        # gym.Env spec attributes:
        self.observation_space = gym.spaces.Box(low=0, high=self.scene.num_radars,
                                                shape=self._observation.shape,
                                                dtype=np.float32)
        self.action_space = gym.spaces.Box(low=-1.0,
                                           high=1.0,
                                           shape=(self.scene.num_radars,),
                                           dtype=np.float32)

        # Fill the spec field with a dummy class to expose the env name
        class PrettyName:
            def __init__(self, name):
                self.id = name

        self.spec = PrettyName(self.__class__.__name__)

    def reset(self, **kwargs):
        self.scene = Scene()
        self.update_observation()
        return self._observation

    def step(self, actions: Sequence[float]) -> Tuple[np.array, float, bool, dict]:
        finished = self.scene.update(actions)
        self.update_observation()
        reward = self.compute_reward()
        return self._observation, reward, finished, self.scene.metrics

    def raster_coordinates(self, pos: Vec2d) -> Tuple[Tuple[int, int, float], ...]:
        """Spreads the given value into the four closest cells of given position.
        Returned coordinates are valid only if related ratio is strictly positive."""

        r = self._grid_size / self.scene.size
        x = pos.x * r
        y = pos.y * r
        x2 = int(x + 0.5)
        x1 = x2 - 1
        y2 = int(y + 0.5)
        y1 = y2 - 1

        if x1 < 0:
            ratio_left = 0
        elif x2 >= self._grid_size:
            ratio_left = 1
        else:
            ratio_left = 0.5 + round(x) - x

        if y1 < 0:
            ratio_top = 0
        elif y2 >= self._grid_size:
            ratio_top = 1
        else:
            ratio_top = 0.5 + round(y) - y

        return ((x1, y1, ratio_left * ratio_top),
                (x1, y2, ratio_left * (1 - ratio_top)),
                (x2, y1, (1 - ratio_left) * ratio_top),
                (x2, y2, (1 - ratio_left) * (1 - ratio_top)))

    def update_observation(self):

        self._observation.fill(0)

        # Encode radars positions and orientation
        # TODO: maybe had better use a Bresenham's line drawing algorithm
        for r, radar in enumerate(self.scene.radars):
            near_point = radar.position + radar.orientation * radar.min_range
            far_point = radar.position + radar.orientation * radar.max_range
            cells = (self.raster_coordinates(radar.position) +
                     self.raster_coordinates(near_point) +
                     self.raster_coordinates(far_point))
            for x, y, ratio in cells:
                if ratio > 0:
                    self._observation[r, 0, y, x] += ratio

        # Encode echos position and radial velocities
        for r, radar in enumerate(self.scene.radars):
            for echo in radar.echos:
                normalized_age = (self.scene.step - echo.date) / self.scene.num_steps
                normalized_velocity = (echo.radial_velocity * (1 - normalized_age) ** 2) / self.scene.plane_max_speed
                for x, y, ratio in self.raster_coordinates(echo.position):
                    if ratio == 0:
                        continue
                    self._observation[r, 1, y, x] += normalized_velocity.x
                    self._observation[r, 2, y, x] += normalized_velocity.y

        # Use corners to write number of steps passed and remaining
        self._observation[0, 0, 0, 0] = self.scene.step
        self._observation[1, 0, 0, 0] = self.scene.num_steps

    def compute_reward(self) -> float:
        reward = 0
        for plane in self.scene.planes:
            max_distance_squared = 2 * self.scene.size ** 2
            if plane.last_seen_position is None:
                distance_squared = max_distance_squared
            else:
                delta = (plane.position - plane.last_seen_position).get_length_sqrd()
                distance_squared = delta
            at_range = False
            for radar in self.scene.radars:
                if radar.at_range(plane.position):
                    at_range = True
                    break
            if not at_range:
                # Ignore plane which are no more at range of any radar
                continue

            # Reward is not normalized by the number of planes to prevent giving information
            # about not yet seen planes in the reward signal
            reward += 1 - distance_squared / max_distance_squared
        return reward / self.scene.num_steps

    def render(self, mode="human"):
        if self._display is None:
            self._display = Display(self.scene)
        else:
            self._display.scene = self.scene
        self._display.render_begin()
        self._display.render_frame()
        self.debug_observation()
        self._display.render_end()
        self._display.handle_events()

    def debug_observation(self):
        radar_colors = [
            pygame.Color(255, 0, 0),
            pygame.Color(0, 255, 0),
            pygame.Color(0, 0, 255),
            pygame.Color(255, 0, 200),
            pygame.Color(0, 192, 250),
        ]

        ratio = self.scene.size / self._grid_size
        cell_size = self._display.view.size2window(ratio)

        for r in range(self.scene.num_radars):
            c = radar_colors[r]
            for y in range(self._grid_size):
                for x in range(self._grid_size):
                    if x == 0 and y == 0:
                        # Cell reserved to encode steps
                        continue

                    rp, vx, vy = self._observation[r, :, y, x]
                    scene_pos = Vec2d(x * ratio, y * ratio)
                    window_pos = self._display.view.point2window(scene_pos)

                    if rp > 0:
                        pygame.draw.rect(self._display.screen,
                                         pygame.Color(c.r, c.g, c.b, int(255 * rp)),
                                         pygame.Rect(window_pos, (cell_size, cell_size)))

                    v = Vec2d(vx, vy)
                    if v.get_length_sqrd() > 0:
                        arrow_end = self._display.view.point2window(scene_pos + v * ratio)
                        pygame.draw.line(self._display.screen, c, window_pos, arrow_end)
                        pygame.draw.circle(self._display.screen, c, window_pos, 2)


class AbsoluteEnv(ContinuousEnv):
    """Variant with absolute target direction passed as actions for each radar."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.action_space = gym.spaces.Box(low=-1.0,
                                           high=1.0,
                                           shape=(self.scene.num_radars, 2),
                                           dtype=np.float32)

    def step(self, actions: np.ndarray) -> Tuple[np.array, float, bool, dict]:
        orders = []
        for (tx, ty), radar in zip(actions, self.scene.radars):
            target = Vec2d(tx + 1, ty + 1) * 0.5 * self.scene.size
            wanted_orientation = target - radar.position
            delta_angle = radar.orientation.get_angle_between(wanted_orientation)
            max_delta_angle = radar.max_angular_speed * self.scene.step_duration
            order = min(1, max(-1, delta_angle / max_delta_angle))
            orders.append(order)

        return super().step(orders)


class DiscreteEnv(ContinuousEnv):
    """Variant with discrete actions space"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.action_space = gym.spaces.MultiDiscrete([3] * self.scene.num_radars)

    def step(self, actions: Sequence[int]) -> Tuple[np.array, float, bool, dict]:
        return super().step([a - 1 for a in actions])
