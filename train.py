"""
Trains and evaluate agent(s) on a discrete benchmark.

Usage: ./train.py <config.gin>
"""
import gin
from docopt import docopt

from rl_utils import train_agent

args = docopt(__doc__)
gin.parse_config_files_and_bindings([args['<config.gin>']], [])
train_agent()
