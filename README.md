# Radar-RL

## Setup

```commandline
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

## Simulator demo

```commandline
$ python display.py
```

## Training

To train an agent, put the whole configuration in a gin-file, and pass it to the "train.py" entry point:
```commandline
$ python train.py <config.gin>
```
This populates a `tb_log` directory with tensorboard logging and the resulting gin configuration 
(in case not every parameter has been set in your config file).

As an example, a random agent baseline is provided:
```commandline
$ python train.py random.gin
```

## Monitoring

To monitor

```commandline
$ tensorboard --logdir tb_log
```

## TODO
- handle transparency in display
- SAC baseline
