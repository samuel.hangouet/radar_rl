import os
import sys
import time
from random import choice
from typing import Dict, List, Optional, Type

import gin
import gym
import numpy as np
from gin import configurable, REQUIRED
from torch.utils.tensorboard import SummaryWriter

from agent import AgentBase
from utils import increment_path


def create_writer(log_path) -> SummaryWriter:
    writer = SummaryWriter(log_path)
    print(f"Logging to {log_path}")
    return writer


@configurable()
def run_episode(agent: AgentBase,
                env: gym.Env,
                training_mode: bool,
                display=False):
    """
    Runs one episode using actions from given agent in given env.
    Returns the dict of final metrics.
    """
    obs = env.reset()
    obs = obs.copy()
    action = agent.begin_episode(obs, training_mode)
    sum_rewards = 0
    length = 0
    while True:
        next_obs, reward, done, metrics = env.step(action)
        if display:
            env.render()
        next_obs = next_obs.copy()
        if training_mode:
            agent.add_training_experience(obs, action, reward, next_obs, done)
        length += 1
        obs = next_obs
        sum_rewards += reward
        if done:
            agent.end_episode(reward, obs)
            break
        action = agent.step(reward, obs)
    assert ('rewards' not in metrics and 'length' not in metrics)
    metrics['rewards'] = sum_rewards
    metrics['length'] = length
    return metrics


def evaluate_agent(agent: AgentBase, env: gym.Env, eval_episodes: int):
    """Evaluates the given agent in the given env for given amount of episodes.
    Returns the list of final metrics for each episode."""

    total_metrics = []
    for run in range(eval_episodes):
        metrics = run_episode(agent, env, False)
        total_metrics.append(metrics)

    return total_metrics


def mean_std_metrics(metrics_iterable):
    """Computes mean and standard deviation of a list of metrics."""

    total_metrics = {}
    for metrics in metrics_iterable:
        for key, value in metrics.items():
            total_metrics.setdefault(key, []).append(value)

    mean = {}
    std = {}
    for key, values in total_metrics.items():
        try:
            float(values[0])
        except (ValueError, TypeError):
            # Cannot average, choose value at random_agent.py
            mean[key] = choice(values)
            continue
        mean[key] = np.mean(values)
        std[key] = np.std(values)

    return mean, std


@configurable
def train_agent(agent_classes: List[Type] = REQUIRED,
                env_class: Type[gym.Env] = REQUIRED,
                average: int = 1,
                episodes: int = None,
                eval_interval: int = 0,
                eval_episodes: int = 0,
                patience_episodes: int = None,
                save_path: str = None,
                load_if_exists: bool = True,
                title: Optional[str] = None) -> Dict[str, List[Dict]]:
    """
    :param agent_classes: the agents to be trained
    :param env_class: the env used for training
    :param average: number of independent training to average (is > 1, the train env will be cloned for each agent)
    :param episodes: max number of training episodes before stopping
    :param eval_interval: frequency of evaluation runs, in number of training actions
    :param eval_episodes: number of episodes used to average for evaluation (if > 0, a clone of env will be made)
    :param patience_episodes: number training episodes without improving the best evaluation score before stopping
    :param save_path: where to save best models
    :param load_if_exists: if save_path is not None, load previous model before training
    :param title: a label to append to tensorboard writer name
    :returns the metrics from the end of each training episodes
    """

    # Instantiate env
    env = env_class()
    eval_env = None

    train_metrics = {}

    for agent_class in agent_classes:

        # Instantiate agent
        agent = agent_class(env.observation_space, env.action_space)

        # Create a path for logging and a tensorboard writer
        report_path = f'tb_log/{env.spec.id}_{agent_class.__name__}'
        if average > 1:
            report_path += f'_mean{average}'
        if title is not None:
            report_path += '_' + title
        report_path = increment_path(f'{report_path}')
        writer = create_writer(report_path)

        # Dump configuration
        with open(report_path + '/config.gin', 'w') as f:
            f.write(gin.operative_config_str())

        start_time = time.time()

        # Count the number of training episodes
        train_episodes = 0

        # Count the number of actions issued in the environment during training
        train_steps = 0

        last_eval_episode = 0
        if eval_interval is not None:
            last_eval_episode = -eval_interval

        best_rewards = None
        best_rewards_episode = None

        if save_path is not None:

            if load_if_exists and os.path.exists(save_path):
                print(f"Loading previous model from {save_path}")
                agent.load(save_path)
            else:
                parent = os.path.dirname(save_path)
                if parent:
                    os.makedirs(parent, exist_ok=True)

        # Duplicate agents and envs to average training results
        train_envs = [env]
        agents = [agent]
        for _ in range(average - 1):
            train_envs.append(env_class())
            agents.append(agent_class(env.observation_space, env.action_space))

        # Sets the writer used by the agent if there is only one
        if len(agents) == 1:
            agents[0].set_writer(writer)

        try:
            while True:
                # Evaluation, checkpoints, early stopping and console log
                if eval_episodes > 0 and eval_interval > 0 and last_eval_episode + eval_interval <= train_episodes:
                    if eval_env is None:
                        eval_env = env_class()
                    last_eval_episode = train_episodes
                    eval_metrics = []
                    for agent in agents:
                        agent_eval_metrics = evaluate_agent(agent, eval_env, eval_episodes)
                        eval_metrics += agent_eval_metrics
                        agent_eval_metrics, _ = mean_std_metrics(eval_metrics)

                        if best_rewards is None or best_rewards < agent_eval_metrics['rewards']:
                            best_rewards = agent_eval_metrics['rewards']
                            best_rewards_episode = train_episodes
                            if save_path is not None:
                                print(f"Saving new best model to {save_path}")
                                agent.save(save_path)

                    if patience_episodes is not None and train_episodes - best_rewards_episode > patience_episodes:
                        print("Early stop")
                        break

                    # Tensorboard eval and console logging
                    eval_metrics_mean, eval_metrics_std = mean_std_metrics(eval_metrics)
                    log_string = f"After {train_episodes} train episodes, eval over {eval_episodes} episodes:"
                    keys = list(eval_metrics_std.keys())
                    keys.sort()
                    for key in keys:
                        mean = eval_metrics_mean[key]
                        std = eval_metrics_std[key]
                        log_string += f" {key}={mean:.2g}({std:.2g})"
                        writer.add_scalar(f'eval/{key}', mean, global_step=train_episodes)
                    print(log_string)
                    sys.stdout.flush()
                    if writer is not None:
                        writer.flush()

                if episodes is not None and train_episodes >= episodes:
                    break

                # Transitions collection
                metrics = []
                for agent, train_env in zip(agents, train_envs):
                    metrics.append(run_episode(agent=agent, env=train_env, training_mode=True))

                metrics, _ = mean_std_metrics(metrics)

                end_time = time.time()
                elapsed = end_time - start_time
                start_time = end_time
                metrics['training_step_time'] = elapsed / metrics['length']

                train_metrics.setdefault(agent_class.__name__, []).append(metrics)
                train_steps += metrics['length']

                # Tensorboard train logging
                writer.add_scalar('train/steps', train_steps, global_step=train_episodes)
                for key, value in metrics.items():
                    writer.add_scalar(f'train/{key}', float(value), global_step=train_episodes)

                train_episodes += 1
        except KeyboardInterrupt:
            print(f"Stop learning of {agent_class.__name__}")
            pass

    return train_metrics
